/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import modelo.Body;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import orm.*;

/**
 * @version 0.0.3,16/04/2017
 * @author Eduard QF
 */
public class VentanaAnotacion extends javax.swing.JFrame {

    /**
     * atributo que almacena la instanciacion ya creada del objeto Body.
     */
    private final Body bd;

    /**
     * atributo que almacena los nombre de los profesores.
     */
    private String[] nOmbprofes;

    /**
     * atributo que almacena los nombres de los alumnos.
     */
    private String[] nombrAlums;

    /**
     * atributo que almacena los detalles de las anotaciones de cada alumno.
     */
    private final String[][] anotaciones;

    /**
     * atributo que almacena los estudiantes del curso.
     */
    Estudiante[] estudiantes;

    /**
     * atributo que almacena los profesores del curso.
     */
    Profesor[] profesores;

    /**
     * atributo que almacena los todos los cursos de la institucion.
     */
    String[] cursos;

    /**
     * constructor clase VentanaAnotacion
     *
     * @param bd Instanciacion del objeto Body
     *
     */
    public VentanaAnotacion(Body bd) {
        this.bd = bd;
        llenarArreglos("1 A");
        cursos = Body.llenarCurso();
        anotaciones = new String[20][3];
        //llenarArreglos((String) jComboBoxCurso.getSelectedItem());
        initComponents();
        llenarTablero((String) jComboBoxAlumnos.getSelectedItem());

    }

    /**
     * metodo encargado de encontrar y guardar una imagen desde la carpeta
     * especificada y guardarla en una variable que luego sera ocupada en las
     * pantallas
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage("C:\\Users\\Eduard QF\\Documents\\NetBeansProjects\\ProgramacionCursosBasica\\src\\img\\images.jpg");
        return retValue;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        addAnotacion = new javax.swing.JButton();
        jComboBoxCurso = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jTextDetalle = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxTipo = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jComboBoxProfesor = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jComboBoxAlumnos = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Anotaciones");
        setIconImage(getIconImage());
        setIconImages(getIconImages());

        addAnotacion.setText("Agregar Anotacion");
        addAnotacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAnotacionActionPerformed(evt);
            }
        });

        jComboBoxCurso.setModel(new javax.swing.DefaultComboBoxModel<>(cursos));
        jComboBoxCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxCursoActionPerformed(evt);
            }
        });

        jLabel1.setText("Curso:");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            anotaciones,
            new String [] {
                "tipo de anotacion", "Profesor", "Detalle"
            }
        ));
        jScrollPane1.setViewportView(jTable2);

        jScrollPane2.setViewportView(jScrollPane1);

        jLabel2.setText("tipo:");

        jComboBoxTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Positiva", "Negativa" }));

        jLabel3.setText("Profesor:");

        jComboBoxProfesor.setModel(new javax.swing.DefaultComboBoxModel<>(nOmbprofes));

        jLabel4.setText("Detalle:");

        jLabel5.setText("Alumno:");

        jComboBoxAlumnos.setModel(new javax.swing.DefaultComboBoxModel<>(nombrAlums));
        jComboBoxAlumnos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jComboBoxAlumnosMousePressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(29, 29, 29)
                        .addComponent(jComboBoxCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBoxAlumnos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jComboBoxProfesor, 0, 118, Short.MAX_VALUE)
                            .addComponent(jComboBoxTipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(addAnotacion)
                        .addGap(0, 50, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextDetalle)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel5)
                    .addComponent(jComboBoxAlumnos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jComboBoxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addAnotacion))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jComboBoxProfesor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextDetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * metodo que crea y almacena una nueva anotacion a trabes de los datos
     * entregados en los campos jText y jComboBox.
     *
     * @param evt evento del boton addAnotacion
     */
    private void addAnotacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAnotacionActionPerformed

        boolean tipo;
        if (jComboBoxTipo.getSelectedItem() == "Positiva") {
            tipo = true;
        } else {
            tipo = false;
        }
        String detalle = jTextDetalle.getText();
        Profesor profesor = bd.compareDocentes(profesores, (String) jComboBoxProfesor.getSelectedItem());
        System.out.println("profesor:" + profesor);
        Estudiante alumno = capanegocios.Read.readEstudiante((String) jComboBoxAlumnos.getSelectedItem());
        System.out.println("alumno:" + alumno.getPersona_id_fk().getRut());
        System.out.println("anotqacion ingresada:" + capanegocios.Create.createAnotacion(detalle, bd.getFecha(), tipo, alumno, profesor));
        JOptionPane.showMessageDialog(this, "anotacion ingresada correctamente: ");
        try {
            bd.guardarCambios();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VentanaAnotacion.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_addAnotacionActionPerformed

    /**
     * metodo encargado de obtener los daros del curso seleccionado en el
     * jComboBoxCurso.
     *
     * @param evt evento que entrega el boton jComboBoxCurso.
     */
    private void jComboBoxCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxCursoActionPerformed
        llenarArreglos((String) jComboBoxCurso.getSelectedItem());
        System.out.println("jComboBoxCurso.getSelectedIndex(): " + jComboBoxCurso.getSelectedIndex());

        jComboBoxAlumnos.removeAllItems();
        for (int i = 0; i < nombrAlums.length; i++) {
            System.out.println("jcombobox i:" + i);
            jComboBoxAlumnos.addItem(nombrAlums[i]);
        }
        jComboBoxProfesor.removeAllItems();
        for (int i = 0; i < nOmbprofes.length; i++) {
            jComboBoxProfesor.addItem(nOmbprofes[i]);
        }
        limpiar();
    }//GEN-LAST:event_jComboBoxCursoActionPerformed

    private void jComboBoxAlumnosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxAlumnosMousePressed
        llenarTablero((String) jComboBoxAlumnos.getSelectedItem());
    }//GEN-LAST:event_jComboBoxAlumnosMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addAnotacion;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> jComboBoxAlumnos;
    private javax.swing.JComboBox<String> jComboBoxCurso;
    private javax.swing.JComboBox<String> jComboBoxProfesor;
    private javax.swing.JComboBox<String> jComboBoxTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextDetalle;
    // End of variables declaration//GEN-END:variables

    /**
     * metodo encargado de llenar los arreglos iniciales de la tabla.
     *
     * @param curso entrega el curso inicial del cual la tabla mostrara sus
     * datos.
     */
    private void llenarArreglos(String idCurso) {
        estudiantes = capanegocios.Read.readEstudiantesCurso(idCurso);
        profesores = capanegocios.Read.readProfesoresCurso(idCurso);
        nOmbprofes = new String[profesores.length];
        nombrAlums = new String[estudiantes.length];
        for (int i = 0; i < nOmbprofes.length; i++) {
            nOmbprofes[i] = profesores[i].getPersona_id_fk().getNombre();
        }

        for (int i = 0; i < nombrAlums.length; i++) {
            nombrAlums[i] = estudiantes[i].getPersona_id_fk().getNombre();
        }

    }

    /**
     * metodeo encargado de modificar los datos que entrega la tabla de
     * anotaciones.
     *
     * @param selectedalumno entrega el alumno seleccionado en el jComboBox del
     * arreglo de alumnos.
     * @param selectedcurso entrega el curso seleccionado en el jComboBox del
     * arreglo de curso.
     */
    private void llenarTablero(String selectedalumno) {
        limpiar();
        String tipo;
        Estudiante alumno = capanegocios.Read.readEstudiante(selectedalumno);
        if (alumno != null) {
            Anotaciones[] anotacion = alumno.anotaciones.toArray();

            if (anotacion.length != 0) {
                for (int i = 0; i < anotacion.length; i++) {
                    System.out.println("i:" + i);
                    if (anotacion[i].getTipo()) {
                        tipo = "positiva";
                    } else {
                        tipo = "Negativa";
                    }
                    jTable2.setValueAt(tipo, i, 0);
                    jTable2.setValueAt(anotacion[i].getProfesor_id_fk().getPersona_id_fk().getNombre(), i, 1);
                    jTable2.setValueAt(anotacion[i].getDetalle(), i, 2);
                }
            }
        }
    }

    /**
     * metodo que elimina todos los datos de la tabla.
     */
    private void limpiar() {
        for (int i = 0; i < jTable2.getRowCount(); i++) {
            for (int j = 0; j < jTable2.getColumnCount(); j++) {
                jTable2.setValueAt("", i, j);
            }
        }

    }
}
