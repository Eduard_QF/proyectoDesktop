package vistas;

import modelo.Body;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import orm.*;

/**
 * @version 0.1.0,20/04/2017
 * @author Eduard QF
 */
public class VentanaAsistencia extends javax.swing.JFrame {

    /**
     * atributo que une la ventana con objeto Body.
     */
    private final Body bd;

    /**
     * atributo en el que almacenamos un arreglo de la asistencia de los
     * alumnos.
     */
    private String[][] asistencia;

    /**
     * atributo que almacena los estudiantes con los que se trabaja.
     */
    private Estudiante[] alumnos;

    /**
     * atributo en el que almacenamos los caracteres que mostrara la tabla.
     */
    private String titulo[];

    /**
     * constructor de la clase Ventena Asistencia.
     *
     * @param bd objeto Body para realizar la coneccion con el el Objeto
     * existente.
     *
     */
    public VentanaAsistencia(Body bd) {
        System.out.println("ventana asistencia");
        this.bd = bd;
        llenarArreglos("1 A");
        initComponents();

    }

    /**
     * metodo encargado de encontrar y guardar una imagen desde la carpeta
     * especificada y guardarla en una variable que luego sera ocupada en las
     * pantallas
     *
     * @return imagen como formato predefinido.
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage("C:\\Users\\Eduard QF\\Documents\\NetBeansProjects\\ProgramacionCursosBasica\\src\\img\\images.jpg");
        return retValue;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jComboBoxCurso = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButtonAddAsistencia = new javax.swing.JButton();
        jButtonGuardar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Asistencia");
        setIconImage(getIconImage());
        setIconImages(getIconImages());

        jLabel1.setText("Curso:");

        jComboBoxCurso.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 A", "1 B","2 A", "2 B","3 A", "3 B","4 A", "4 B","5 A", "5 B","6 A", "6 B","7 A", "7 B","8 A", "8 B"}));
        jComboBoxCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxCursoActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            asistencia,
            titulo
        ));
        jScrollPane1.setViewportView(jTable1);

        jButtonAddAsistencia.setText("Añadir asistencia");
        jButtonAddAsistencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddAsistenciaActionPerformed(evt);
            }
        });

        jButtonGuardar.setText("Guardar cambios");
        jButtonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGuardarActionPerformed(evt);
            }
        });

        jLabel2.setText("o: Precente x:Ausente");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBoxCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(jButtonAddAsistencia)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonGuardar)
                        .addGap(303, 303, 303)
                        .addComponent(jLabel2)
                        .addGap(0, 253, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jComboBoxCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonAddAsistencia)
                        .addComponent(jButtonGuardar))
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * metodo que cambia los valores dentro de jTable1.
     *
     * @param evt entrega el evento del boton jComboBox1.
     */
    private void jComboBoxCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxCursoActionPerformed
        limpiar();
        alumnos = capanegocios.Read.readEstudiantesCurso((String) jComboBoxCurso.getSelectedItem());
        for (int i = 0; i < alumnos.length; i++) {
            jTable1.setValueAt(alumnos[i].getPersona_id_fk().getNombre(), i, 0);
            Asistencia[] asiss = alumnos[i].asistencia.toArray();
            for (int j = 1; j <= asiss.length; j++) {
                if (asiss[j - 1].getAsis()) {
                    jTable1.setValueAt(("o"), i, j);
                } else if (asiss[j - 1].getAsis()) {
                    jTable1.setValueAt(("x"), i, j);
                } else {
                    jTable1.setValueAt((" "), i, j);
                }

            }
        }
    }//GEN-LAST:event_jComboBoxCursoActionPerformed

    /**
     * metodo encargado de añadir una nueva columna a la tabla de asistencia.
     *
     * @peram evt entrega el evento del boton agregar Asistencia.
     */
    private void jButtonAddAsistenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddAsistenciaActionPerformed
        int precent;
        boolean asis;
        for (int i = 0; i < asistencia.length; i++) {
            precent = JOptionPane.showOptionDialog(this, asistencia[i][0], "Asistencia", JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Presente", "Ausente"}, "");
            asis = precent == 0;
            System.out.println("asis: " + asis);
            Estudiante alumno;
            capanegocios.Create.createAsistencia(asis, bd.getFecha(), alumnos[i]);
            //in.newAsistencia(jComboBoxCurso.getSelectedIndex(), i, rootPaneCheckingEnabled, bd.getFecha());
        }
    }//GEN-LAST:event_jButtonAddAsistenciaActionPerformed

    /**
     * metodo encargado de agregar los datos nuevos de la tabla a cada alumno
     * estipulado en esta.
     *
     * @param evt entrega el evento del boton Guardar.
     */
    private void jButtonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGuardarActionPerformed

        try {
            bd.guardarCambios();
            dispose();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VentanaAsistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButtonGuardarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddAsistencia;
    private javax.swing.JButton jButtonGuardar;
    private javax.swing.JComboBox<String> jComboBoxCurso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    /**
     * metollo que llena por primera ves los arreglos que contienen los primeros
     * datos a ser mostrados por la tabla
     *
     * @param curso entrega el primer curso en aparecer los datos en la tabla;
     */
    private void llenarArreglos(String idCurso) {
        alumnos = capanegocios.Read.readEstudiantesCurso(idCurso);
        Asistencia[] asiss = capanegocios.Read.readAsistencias(alumnos[0].getPersona_id_fk().getRut());
        titulo = new String[asiss.length];
        asistencia = new String[alumnos.length][(asiss.length + 1)];
        titulo[0] = "Estudiante";
        for (int i = 0; i < alumnos.length; i++) {
            System.out.println("-----------" + i + "-------------");
            asistencia[i][0] = alumnos[i].getPersona_id_fk().getNombre();
            asiss = alumnos[i].asistencia.toArray();
            for (int j = 1; j < asiss.length; j++) {
                if (titulo[titulo.length - 1] == null) {
                    //titulo[j] = in.getCursos().get(curso).getAlumnos().get(0).getAsistencia().get(j - 1).getTime();
                    titulo[j] = asiss[j - 1].getFecha();
                }
                System.out.println("nombre:" + alumnos[i].getPersona_id_fk().getNombre() + "\tasistencia:" + String.valueOf(asiss[j - 1].getAsis()));
                if (asiss[j - 1].getAsis()) {
                    asistencia[i][j] = "o";
                } else {
                    asistencia[i][j] = "x";
                }

            }
        }
    }

    /**
     * metodo dedicado a la limpieza de la tabla remplazando los datos por
     * espacios vacios.
     */
    private void limpiar() {
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            for (int j = 0; j < jTable1.getColumnCount(); j++) {
                jTable1.setValueAt("", i, j);
            }
        }
    }

}
