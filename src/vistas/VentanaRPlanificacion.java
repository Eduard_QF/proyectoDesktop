package vistas;


import datos.CreadorReportes;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Body;
import org.orm.PersistentException;
import orm.*;

/**
 *
 * @author Eduard QF
 */
public class VentanaRPlanificacion extends javax.swing.JFrame {

    /**
     * metodo que al almacenara la instanciación del objeto Body
     */
    private final Body bd;

    /**
     * atributo que almacenara los nombre de los apoderados desde lista cuando
     * ya se encuantren ordenados y sin repetir.
     */
    private String lista[];

    /**
     * lista que recibe hordena y elimina los apoderados inscritos en la
     * institucion a traves de la libreria HasSet().
     * @deprecated 
     */
    private final Set<Apoderado> list = new HashSet<>();

    /**
     * constructor de la clase VentanaRPlanificacion.
     *
     * @param bd instanciacion ya echa del objeto Body.

     */
    public VentanaRPlanificacion(Body bd){
        this.bd = bd;
        llenarJBOx();
        initComponents();
    }

    /**
     * metodo encargado de encontrar y guardar una imagen desde la carpeta
     * especificada y guardarla en una variable que luego sera ocupada en las
     * pantallas
     * @return imagen en formato predefinido.
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage("C:\\Users\\Eduard QF\\Documents\\NetBeansProjects\\ProgramacionCursosBasica\\src\\img\\images.jpg");
        return retValue;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Planificaion Pupilos");
        setIconImage(getIconImage());
        setIconImages(getIconImages());

        jButton1.setText("generar Reporte");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(lista));

        jLabel1.setText("Seleccione Apoderado:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addComponent(jButton1)))
                .addContainerGap(48, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addGap(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * metodo que se activa al recivir una accion el boton de generar reporte y
     * llama al metodo requerido para esto.
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            CreadorReportes cr = new CreadorReportes(bd);
            cr.reportePlanificacion((String)jComboBox1.getSelectedItem());
            dispose();
        } catch (PersistentException ex) {
            Logger.getLogger(VentanaRPlanificacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * metodo que llena los datos que contendra el jcomboBox.
     */
    private void llenarJBOx(){
        
        for (int i=0;i<16;i++) {
            list.addAll(Arrays.asList(capanegocios.Read.readApoderadosCurso(bd.getIdcurso(i))));
        }
        lista = new String[list.size()];
        System.out.println("largo de apoderados: " + list.size());
        int cont = 0;
        for (Apoderado ap : list) {
            lista[cont] = ap.getPersona_id_fk().getNombre();
            cont++;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
